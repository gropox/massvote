const steem = require("golos-js");
const info = require("debug")("massvote:inf");
const debug = require("debug")("massvote:dbg");
const trace = require("debug")("massvote:trc");
const error = require("debug")("massvote:err");
const fs = require("fs");

const CONFIG_FILE = "./config.json";
const VOTERS_FILE = "./voters.json";

const CONFIG = JSON.parse(fs.readFileSync(CONFIG_FILE, "utf8"));
const VOTERS = JSON.parse(fs.readFileSync(VOTERS_FILE, "utf8"));

steem.config.set('websocket', CONFIG.websocket);
steem.config.set('address_prefix', CONFIG.address_prefix);
steem.config.set('chain_id', CONFIG.chain_id);

const VOTE_QUEUE = [];
const VOTER_QUEUE = [];

let current_vote = null;

function init() {
    for(let v of Object.keys(VOTERS)) {
        VOTER_QUEUE.push(v);
    }
    shuffle(VOTER_QUEUE);
}

async function processPost(post) {
    info("process post " + post.author + "/" + post.permlink);

    for(let i = 0; i < CONFIG.voters_per_post; i++) {
        debug("get next voter");
        if(VOTER_QUEUE.length > 0) {
            const vote = new VoteTask(VOTER_QUEUE.shift(), post.author, post.permlink);
            VOTE_QUEUE.push(vote);
        } else {
            info("VOTE_QUEUE is empty");
            break;
        }
    }
}

async function processVotes() {
    const NUMVOTES = VOTE_QUEUE.length;
    for(let i = 0; i < NUMVOTES; i++) {
        current_vote = VOTE_QUEUE.shift();
        if(await current_vote.vote()) {
            debug("voter " + current_vote.voter + " voted, push back into the voter queue");
            VOTER_QUEUE.push(current_vote.voter);
        } else {
            trace("not voted, push back into vote queue");
            VOTE_QUEUE.push(current_vote);
        }
        current_vote = null;
    }
}

/* @@ -2244,26 +2244,124 @@ */
function isDiff(body) {
    return body.match(/^@@[0-9, +-]+@@/);
}

async function processBlock(bn) {
        info("processing block " + bn);
        let lastTime = 0;
        let transactions = await steem.api.getOpsInBlockAsync(bn, false);
        //log.debug(JSON.stringify(transactions));
        for(let tr of transactions) {
            trace("tr " + tr.trx_in_block);
            lastTime = Date.parse(tr.timestamp);
            let op = tr.op[0];
            let opBody = tr.op[1];
            switch(op) {
                case "comment":

                    if(opBody.parent_author == "" && !isDiff(opBody.body)) {
                        debug("found post " + opBody.author + "/" + opBody.permlink);
                        const content = await steem.api.getContentAsync(opBody.author, opBody.permlink);
                        debug("content permlink = " + content.permlink);
                        debug("content created = " + content.created);
                        debug("content last_update = " + content.last_update);
                        debug("content diff = " + Math.abs(Date.parse(content.created) - Date.parse(content.last_update)));
                        
                        if(content.permlink == opBody.permlink &&
                            3000 >= Math.abs(Date.parse(content.created) - Date.parse(content.last_update))) {
                                if(await repOk(content.author)) {
                                    await processPost(content);
                                }
                        }
                    }

            }
        }
        return lastTime;
}

async function run() {
    init();
    let props = await steem.api.getDynamicGlobalPropertiesAsync();
    let block = props.head_block_number - 3;
    //block = 8767268;
    info("start looping with block " + block);
    let startTime = Date.now();
    let startBlock = block; 
    while(true) {
        try {
            info("voters ready vor voting " + VOTER_QUEUE.length);
            info("votes in pipeline " + VOTE_QUEUE.length);
            if(block >= props.head_block_number) {
                props = await steem.api.getDynamicGlobalPropertiesAsync();
                await sleep(1500);
                continue;
            }
            await processBlock(block++);
            const timeElapsedBlocks = (block - startBlock) * 3 * 1000;
            const timeElapsed = Date.now() - startTime; 
            debug("timediff = " + Math.abs(timeElapsedBlocks - timeElapsed) / 1000);
            if(Math.abs(timeElapsedBlocks - timeElapsed) > (1000*60*5)) {
                info("Delay is to big, reset block");
                props = await steem.api.getDynamicGlobalPropertiesAsync();
                block = props.head_block_number;
                startBlock = block;
                startTime = Date.now();
            }
            await processVotes();
        } catch(e) {
            error("Error catched in main loop!");
            error(getExceptionCause(e));
            if(current_vote) {
                VOTER_QUEUE.push(current_vote.voter);
                current_vote = null;
            }
        }
        await sleep(500);
    }
    process.exit(1);
}

async function vote(voter, key, author, permlink, weight) {
    await steem.broadcast.voteAsync(key, voter, author, permlink, weight);
}

async function repOk(userid) {
    const akk = await getAccount(userid);
    const rep = repLog10(akk.reputation);
    debug(userid + " rep = " + rep.toFixed(1));
    
    return (CONFIG.min_reputation <= rep);
}


function log10(str) {
    const leadingDigits = parseInt(str.substring(0, 4));
    const log = Math.log(leadingDigits) / Math.LN10 + 0.00000001
    const n = str.length - 1;
    return n + (log - parseInt(log));
}

function repLog10(rep2) {
    if(rep2 == null) return rep2
    let rep = String(rep2)
    const neg = rep.charAt(0) === '-'
    rep = neg ? rep.substring(1) : rep

    let out = log10(rep)
    if(isNaN(out)) out = 0
    out = Math.max(out - 9, 0); // @ -9, $0.50 earned is approx magnitude 1
    out = (neg ? -1 : 1) * out
    out = (out * 9) + 25 // 9 points per magnitude. center at 25
    // base-line 0 to darken and < 0 to auto hide (grep rephide)
    return out
}

async function getAccount(userid) {
    for(let t = 0; t < 1; t++) {
        //log.debug("\tget acc user " + userid);
        var users = await steem.api.getAccountsAsync([userid]);
        if(users && users.length > 0) {
            return users[0];
        } 
    }
    return null;
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

function getExceptionCause(e) {
    if(e.cause && e.cause.payload && e.cause.payload.error) {
        let m = e.cause.payload.error.message; 
        if(m) {
            let am = m.split("\n");
            m = am[0];
            for(let i = 1; i < am.length && i < 3; i++) {
                m += ": " + am[i];
            }
            return m;
        }
    }
    return e;
}

class VoteTask {
    constructor(voter, author, permlink) {
        this.voter = voter;
        this.author = author;
        this.permlink = permlink;
        this.voteTime = voteTime();
        debug("add vote " + this.voter + " for " + this.author + "/" + this.permlink);
    }

    async vote() {
        trace("try vote " + this.voter + " for " + this.author + "/" + this.permlink + " " + Math.abs(Date.now() - this.voteTime));
        if(Date.now() >= this.voteTime) {
            if(CONFIG.broadcast) {
                info("mature vote " + this.voter + " " + this.author + " " + this.permlink + " " + 10000);
                await steem.broadcast.voteAsync(VOTERS[this.voter], this.voter, this.author, this.permlink, 10000);
            } else {
                debug("no broadcast, should be voted");
            }
            trace("return true");
            return true;
        }
        return false;
    }
}


function voteTime() {
    const rndSeconds = getRandomArbitrary(CONFIG.min_vote_delay, CONFIG.max_vote_delay);
    return Date.now() + rndSeconds * 1000;
}

function voteDelay() {
    const rndSeconds = getRandomArbitrary(CONFIG.min_vote_delay, CONFIG.max_vote_delay);
    return Date.now() + rndSeconds * 1000;
}

function getRandomArbitrary(min, max) {
  return Math.random() * (max - min) + min;
}

function shuffle(array) {
  var currentIndex = array.length, temporaryValue, randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}

run();